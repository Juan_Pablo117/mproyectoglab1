/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogit1;
import java.util.Scanner;

/**
 *
 * @author pista
 */
public class MediaSuma {
        public static void imprimirMensaje(String sMensaje){
        System.out.println(sMensaje);
    }
    static void encabezado(){
        imprimirMensaje("Universidad Autónoma de Campeche");
        imprimirMensaje("Facultad de Ingeniería");
        imprimirMensaje("Ingeniería en Sistemas Computacionales");
        imprimirMensaje("Lenguaje de Programación 2 - A");
        imprimirMensaje("Juan Pablo Diaz León - 63832");
        separador();
    }
    static void piepagina(){
        separador();
        imprimirMensaje("(c) UACAM-FI-ISC-LP2-A");
    }    
    public static void separador(){
        imprimirMensaje("-------------------------------------");
    }
    public static void MediaSuma(){
        double Media = 0;
        double Suma = 0;
        Scanner entrada = new Scanner(System.in);
        imprimirMensaje("Ingresa un número para calcular la media: ");
        double Valor = entrada.nextInt();
        for (double i=0; i<=Valor; i++){
            Suma = Suma + i;
            imprimirMensaje("La suma total de los números es: " + Suma);
            Media = Suma/Valor;
            imprimirMensaje("La media total es: " + Media);
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        encabezado();
        MediaSuma();
        piepagina();
    }
}
