/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogit1;
import java.util.Scanner;
/**
 *
 * @author pista
 */
public class KinderCriCri {

    /**
     * @param args the command line arguments
     */
    static void imprimirMensaje(String sMensaje){
        System.out.println(sMensaje);
    }
static void separador(){
        imprimirMensaje("-----------------------------------------");
    }
    
    static void encabezado(){
        imprimirMensaje("Universidad Autónoma de Campeche");
        imprimirMensaje("Facultad de Ingeniería");
        imprimirMensaje("Ingeniería en Sistemas Computacionales");
        imprimirMensaje("Lenguaje de Programación 2 - A");
        imprimirMensaje("Juan Pablo Diaz León - 63832");
        separador();
    }
static void piepagina(){
        separador();
        imprimirMensaje("(c) UACAM-FI-ISC-LP2-A");
    }    
static void validar(){
        Scanner sc = new Scanner( System.in );
        imprimirMensaje("Ingrese una vocal mayúscula o minúscula: ");
        String letra = sc.next();
        if ("A".equals(letra)){
            imprimirMensaje("Felicidades, es una vocal mayúscula");
        } else { if ("a".equals(letra)){
            imprimirMensaje("Felicidades, es una vocal minúscula");
        } else { imprimirMensaje (" Lo siento, no es una vocal");
        }
        } 
}
    public static void main(String[] args) {
        // TODO code application logic here
        /*5.- El siguiente algoritmo fue solicitado por el kinder "CriCri",
        el cual debe ayudar en el aprendizaje de las vocales minúsculas y mayúsculas,
        se debe leer una letra, el algoritmo debe determinar si es una vocal 
        minúscula o mayúscula, si es así debe desplegar un mensaje de Felicitación
        indicando si fue Mayúscula o minúscula, de lo contrario debe indicarle que no
        es una vocal. Validad con la letra A.a*/
        encabezado();
        validar();
        piepagina();
    }  
}
