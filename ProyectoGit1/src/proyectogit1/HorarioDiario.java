/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogit1;
import java.util.Scanner;
/**
 *
 * @author pista
 */
public class HorarioDiario {
    static void encabezado(){
        imprimirMensaje("Universidad Autónoma de Campeche");
        imprimirMensaje("Facultad de Ingeniería");
        imprimirMensaje("Ingeniería en Sistemas Computacionales");
        imprimirMensaje("Lenguaje de Programación 2 - A");
        imprimirMensaje("Juan Pablo Diaz León - 63832");
        separador();
    }
    static void piepagina(){
        separador();
        imprimirMensaje("(c) UACAM-FI-ISC-LP2-A");
    }    
    public static void imprimirMensaje(String sMensaje){
        System.out.println(sMensaje);
    }
    public static void separador(){
        imprimirMensaje("-------------------------------------");
    }
public static void opciones_switch(int opcion){
        switch (opcion){
            case 1:
                imprimirMensaje("6:00 am, despertar.");
                break;
            case 2:
                imprimirMensaje("6:10 am, tomar un baño.");
                break;
            case 3:
                imprimirMensaje("6:15 am, sacar al perro al patio y darle de comer.");
                break;
            case 4:
                imprimirMensaje("6:30 am, desayunar.");
                break;
            case 5:
                imprimirMensaje("6:50 am, prepararme para la primera clase en línea.");
                break;
            case 6:
                imprimirMensaje("7:00 - 9:00 am, presentar mi primera clase: lenguaje de programación 1.");
                break;
            case 7:
                imprimirMensaje("9:00 - 11:00 am, presentar mi segunda clase: física.");
                break;
            case 8:
                imprimirMensaje("11:00 am, comer alguna fruta y tomar agua.");
                break;
            case 9:
                imprimirMensaje("11:15 am - 1:00 pm, aprovechar el tiempo libre para adelantar alguna tarea pendiente.");
                break;
            case 10:
                imprimirMensaje("1:00 - 3:00 pm, presentar mi tercera y última clase: organización computacional.");
                break;
            case 11:
                imprimirMensaje("3:00 pm, almorzar en casa de mis abuelos.");
                break;
            case 12:
                imprimirMensaje("3:20 pm, dirigirme a casa y darle de comer al perro.");
                break;
            case 13:
                imprimirMensaje("4:00 pm, hacer mis tareas del dia pendiente.");
                break;
            case 14:
                imprimirMensaje("5:00 pm, comer alguna fruta y tomar agua.");
                break;
            case 15:
                imprimirMensaje("5:15 pm, sacar a pasear al perro en algún área segura.");
                break;
            case 16:
                imprimirMensaje("6:00 pm,regresar a casa ");
                break;
            case 17:
                imprimirMensaje("6:20 pm, tomar un baño");
                break;
            case 18:
                imprimirMensaje("6:40 pm, tener un rato de ocio en mi celular o computadora.");
                break;
            case 19:
                imprimirMensaje("7:30 pm, limpiar el patio de los desechos del perro.");
                break;
            case 20:
                imprimirMensaje("8:30 pm, cenar.");
                break;
            case 21:
                imprimirMensaje("9:00 pm, practicar mis ejercicios de fisioterapia.");
                break;
            case 22:
                imprimirMensaje("10:00 pm, darle de comer al perro pero en menor cantidad.");
                break;
            case 23:
                imprimirMensaje("10:30 pm, leer algún libro preferido que tengo pendiente a terminar.");
                break;
            case 24:
                imprimirMensaje("11:30 pm, preparar mis cosas para el día siguiente e irme a dormir.");
                break;
            default:
                imprimirMensaje("Opción no valida, por favor ingresa un valor válido dentro del rango.");
                break;    
        }        
   }    
    public static void main(String[] args) {
        // TODO code application logic here
        encabezado();
        imprimirMensaje("Horario de un dia cotidiano: Martes ");
        imprimirMensaje("Para desplegar una opción, por favor introducir un valor del 1 al 24. ");
        separador();
        Scanner entrada = new Scanner( System.in );
        System.out.print("Leer opción : ");        
        int iValor1 = entrada.nextInt();
        opciones_switch(iValor1);
        piepagina();
    } 
}
