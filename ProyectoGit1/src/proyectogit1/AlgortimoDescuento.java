/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogit1;
import java.util.Scanner;

/**
 *
 * @author pista
 */
public class AlgortimoDescuento {

    /**
     * @param args the command line arguments
     */
    static void imprimirMensaje(String sMensaje){
        System.out.println(sMensaje);
    }
static void separador(){
        imprimirMensaje("-----------------------------------------");
}
    static void encabezado(){
        imprimirMensaje("Universidad Autónoma de Campeche");
        imprimirMensaje("Facultad de Ingeniería");
        imprimirMensaje("Ingeniería en Sistemas Computacionales");
        imprimirMensaje("Lenguaje de Programación 2 - A");
        imprimirMensaje("Juan Pablo Diaz León - 63832");
        separador();
    }
static void piepagina(){
        separador();
        imprimirMensaje("(c) UACAM-FI-ISC-LP2-A");
    }    
static void descuento(){
    Scanner sc = new Scanner ( System.in);
    imprimirMensaje("Ingresa la cantidad de productos a comprar: ");
    int CP = sc.nextInt();
    imprimirMensaje("Ingresa el precio del producto a comprar");
    int PP = sc.nextInt();
    double PF = (CP * PP);
    imprimirMensaje("El valor de la compra es: "+PF+" pesos");
    if (PF < 50){
        double D1 = (PF * 0.08);
        double PCD = (PF - D1);
        imprimirMensaje("Como el valor de la compra es menor a 50 pesos, se aplicará un descuento del 8%,"
                + " el descuento será de: "+D1+" pesos, y el nuevo precio será: "+PCD+" pesos.");
    }else 
        if (PF >= 50){
            double D1 = (PF * 0.1);
            double PCD = (PF - D1);
            imprimirMensaje("Como el valor de la compra es igual o mayor a 50 pesos, se aplicará un descuento del 10%,"
                    + " el descuento será de: "+D1+" pesos, y el nuevo precio será: "+PCD+" pesos.");
        }
}
    public static void main(String[] args) {
        // TODO code application logic here
        /*15.- En una tienda se ha establecido la siguiente oferta: por compras menores a 50
          pesos se hace un descuento de 8%, pero para compras a partir de 50 el descuento
          es de 10%. Se pide ingresar la cantidad y el precio del producto que se compra y
          determinar cuanto se descontará y cuanto se cobrará.*/
        encabezado();
        descuento();
        piepagina();
    } 
}
